# osx-el-capitan-theme-git

Theme mimics OS X 10.12 macOS Sierra, formaly osx-el-capitan-theme, for GTK3 and some DEs (GNOME Shell, Xfce, Cinnamon)

https://github.com/Elbullazul/macOS-Sierra

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/themes-and-icons/osx-el-capitan-theme-git.git
```

